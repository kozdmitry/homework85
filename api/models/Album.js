const mongoose = require("mongoose");

const AlbumSchema = new mongoose.Schema ({
    title: {
      type: String,
      required: true,
      unique: true,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Artist",
        required: true,
    },
    image: {
        type: String,
    },
    year: {
        type: Date,
    }
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;