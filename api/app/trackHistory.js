const express = require("express");
const TrackHistory = require("../models/TrackHistory");
const User = require("../models/User");
const Track = require("../models/Track");
const auth = require("../middleware/auth");


const router = express.Router();


router.get("/", async (req, res) => {
    try {
        const trackHistory = await TrackHistory.find({ user: req.query.user })
            .populate({path: "track"})
            .sort({ datetime: 1 });
        return res.send(trackHistory);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, async(req, res) => {

    try{
        const token = req.get('Authorization');

        if(!token) {
            return res.status(401).send({error: 'No token present'});
        }

        const user = await User.findOne({token});

        if(!user) {
            return res.status(401).send({error: 'Wrong token!'});
        }

        const track = await Track.findOne({ _id: req.body.track });
        const trackHistory =  new TrackHistory({user: user._id, track: track._id});
        trackHistory.generateDate();
        await  trackHistory.save();

        return res.send(trackHistory);

    }catch (e) {
        return res.sendStatus(500);
    }
});

module.exports = router;

