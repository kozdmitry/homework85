import {
    FETCH_ALBUMS_FAILURE,
    FETCH_ALBUMS_SUCCESS, FETCH_ARTIST_FAILURE,
    FETCH_ARTIST_REQUEST,
    FETCH_ARTIST_SUCCESS, FETCH_TRACKS_FAILURE, FETCH_TRACKS_REQUEST, FETCH_TRACKS_SUCCESS
} from "../actions/artistsActions";

const initialState = {
    artists: [],
    albums: [],
    tracks: [],
    loading: false,
    error: false
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_REQUEST:
            return {...state, loading: true};
        case FETCH_ARTIST_SUCCESS:
            return {...state, loading: false, artists: action.artists};
        case FETCH_ARTIST_FAILURE:
            return {...state, loading: false};
        case FETCH_ALBUMS_SUCCESS:
            return { ...state, loading: false, albums: action.albums };
        case FETCH_ALBUMS_FAILURE:
            return { ...state, loading: false, error: action.error };
        case FETCH_TRACKS_REQUEST:
            return { ...state, loading: true };
        case FETCH_TRACKS_SUCCESS:
            return { ...state, loading: false, tracks: action.tracks };
        case FETCH_TRACKS_FAILURE:
            return { ...state, loading: false, error: action.error };
        default:
            return state;
    }
};

export default artistsReducer;