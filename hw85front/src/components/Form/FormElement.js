import React from 'react';
import {Grid, MenuItem, TextField} from "@material-ui/core";
import PropTypes from 'prop-types';



const FormElement = ({error, select, options, ...props}) => {
    let inputChildren = null;

    if (select) {
        inputChildren = options.map((option) => (
            <MenuItem key={option._id} value={option._id}>
                {option.title}
            </MenuItem>
        ));
    }

    return (
        <Grid item xs>
            <TextField select={select} error={Boolean(error)} helperText={error} {...props}>
                {inputChildren}
            </TextField>
        </Grid>
    );
};

FormElement.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    type: PropTypes.string,
    error: PropTypes.string
};
export default FormElement;