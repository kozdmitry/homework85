const mongoose = require ("mongoose");

const TrackHistorySchema = new mongoose.Schema ({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    datetime: {
        type: String,
    },
    track: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Track",
        required: true,
    },
});

TrackHistorySchema.methods.generateDate = function () {
    this.datetime = new Date().toISOString();
};

const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);
module.exports = TrackHistory;