import React from "react";
import ReactDOM from "react-dom";
import {Router} from 'react-router-dom';
import App from "./App";
import {MuiThemeProvider} from "@material-ui/core";
import {Provider} from "react-redux";
import {NotificationContainer} from 'react-notifications';
import history from "./history";
import store from "./store/configureStore";
import theme from "./theme";


const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <NotificationContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);
ReactDOM.render(app, document.getElementById("root"));