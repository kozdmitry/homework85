const mongoose = require ("mongoose");

const TruckSchema = new mongoose.Schema ({
    title: {
        type: String,
        required: true,
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Album",
        required: true,
    },
    number: {
        type: Number,
        required: true,
    },
    published: {
        type: String,
        required: true,
        default: false,
        enum: [true, false],
    },
    timing: {
        type: String
    },
});

const Track = mongoose.model('Track', TruckSchema);
module.exports = Track;
