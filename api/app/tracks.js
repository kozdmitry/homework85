const express = require("express");
const Track = require("../models/Track");
const auth = require("../middleware/auth");

const router = express.Router();

router.get("/:id", async (req, res) => {
    try {
        const track = await Track.findOne({ _id: req.params.id })
            .populate("album", "title description");
        return res.send(track);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/", async (req, res) => {
    try {
        if(req.query.album) {
        const track = await Track.find({ albums: req.query.albums })
            .populate("album", "title", )
            .sort({ number: 1 });
        return res.send(track);
    }

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    const trackInfo = req.body;
    try {
        const track = new Track (trackInfo);
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send(e);
    }
    return res.sendStatus(500);
});

module.exports = router;
