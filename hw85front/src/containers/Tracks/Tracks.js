import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks} from "../../store/actions/artistsActions";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TracksItem from "./TracksItem";

const Tracks = (props) => {
    const dispatch = useDispatch();
    const tracks = useSelector((state) => state.artists.tracks);
    console.log(tracks);

    useEffect(() => {
        dispatch(fetchTracks(props.match.params.id));
    }, [dispatch, props.match.params.id])

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container spacing={1}>
                {tracks.map((track) => (
                    <TracksItem
                        key={track._id}
                        _id={track._id}
                        title={track.title}
                        timing={track.timing}
                        number={track.number}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Tracks;