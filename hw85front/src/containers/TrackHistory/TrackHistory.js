import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/trackHistoryActions";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Paper} from "@material-ui/core";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const trackHistory = useSelector(state => state.trackHistory.trackHistory);

    useEffect(() => {
        dispatch(fetchTrackHistory())
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography/>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {trackHistory.map((history) => (
                    <Paper>
                        <p>Название Трека {history.track}</p>
                        <p>Дата: {history.datetime}</p>
                    </Paper>
                ))}
            </Grid>
        </Grid>
    );
};

export default TrackHistory;