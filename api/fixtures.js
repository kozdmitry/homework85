const mongoose = require("mongoose");
const config = require("./config");
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const { nanoid } = require("nanoid");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [eminem, cent, leps] = await Artist.create(
        {
            title: "Eminem",
            image: "fixtures/eminem.jpg",
            published: "published",
        },
        {
            title: "50 Cent",
            image: "fixtures/50cent.jpg",
            published: "published" },
        {
            title: "Лепс",
            image: "fixtures/leps.png",
            published: "published"}
    );

    const [theBest, show, get] = await Album.create(
        {
            title: "The Best",
            artist: leps,
            image: "fixtures/thebest.jpg",
            published: "published",
            date: 2016,
        },
        {
            title: "The Eminem Show",
            artist: eminem,
            image: "fixtures/eminemshow.jpg",
            published: "published",
            date: 2000,
        },
        // {
        //     title: "The slim shady",
        //     artist: eminem,
        //     image: "fixtures/slimshady.jpeg",
        //     published: "published",
        //     date: 1999,
        // },
        {
            title: "Get Rich or Die Tryin",
            artist: cent,
            image: "fixtures/GetRich.jpg",
            published: "published",
            date: 2012,
        }
    );

    await Track.create(
        { title: "Ну и что", timing: "5:40", number: 1, published: true, album: theBest},
        { title: "Натали", timing: "5:50", number: 2, published: true, album: theBest},
        { title: "Спокйной ночи Господа", timing: "5:39", number: 3, published: true, album: theBest},
        { title: "Расскажи", timing: "4:50", number: 4, published: true, album: theBest},

        { title: "Slim shady", timing: "5:00", number: 1, published: true, album: show},
        { title: "Sing For the Moment", timing: "5:39", number: 2, published: true, album: show},
        { title: "Without Me", timing: "4:50", number: 3, published: true, album: show},
        { title: "White America", timing: "5:24", number: 4, published: true, album: show},

        { title: "Many man", timing: "4:06", number: 1, published: true, album: get},
        { title: "In da club", timing: "5:06", number: 2, published: true, album: get},
        { title: "P.I.M.P", timing: "3:06", number: 3, published: true, album: get},
        { title: "Get Rich Or Die Tryin", timing: "6:06", number: 4, published: true, album: get},

    );

    await User.create(
        {
            username: "Vasiliy",
            password: "123123",
            token: nanoid(),
            role: "user",
        },
        {
            username: "admin",
            password: "123456",
            token: nanoid(),
            role: "admin",
        }
    );

    await mongoose.connection.close();
};

run().catch(console.error);
