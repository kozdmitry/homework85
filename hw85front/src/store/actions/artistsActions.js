import axiosApi from "../../axiosApi";

export const FETCH_ARTIST_REQUEST = 'FETCH_ARTIST_REQUEST';
export const FETCH_ARTIST_SUCCESS = 'FETCH_ARTIST_SUCCESS';
export const FETCH_ARTIST_FAILURE = 'FETCH_ARTIST_FAILURE';

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';


export const fetchArtistRequest = () => ({type: FETCH_ARTIST_REQUEST});
export const fetchArtistSuccess = artists => ({type: FETCH_ARTIST_SUCCESS, artists});
export const fetchArtistFailure = () => ({type: FETCH_ARTIST_FAILURE});

export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
export const fetchAlbumsFailure = () => ({type: FETCH_ALBUMS_FAILURE});

export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});
export const fetchTracksFailure = () => ({type: FETCH_TRACKS_FAILURE});


export const fetchArtists = () => {
    return async dispatch => {
        try {
            dispatch(fetchArtistRequest());
            const response = await axiosApi.get('/artists');
            dispatch(fetchArtistSuccess(response.data));
        } catch (e) {
            dispatch(fetchArtistFailure());
        }
    }
};

export const fetchAlbums = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchAlbumsRequest());
            const response = await axiosApi.get('/albums?artist=' + id);
            dispatch(fetchAlbumsSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumsFailure());
        }
    }
};
export const fetchTracks = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchTracksRequest());
            const response = await axiosApi.get('/tracks?albums=' + id);
            dispatch(fetchTracksSuccess(response.data));
        } catch (e) {
            dispatch(fetchTracksFailure());
        }
    }
};




