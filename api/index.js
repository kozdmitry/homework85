const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const exitHook = require('async-exit-hook');
const albums = require("./app/albums");
const artists = require("./app/artists");
const tracks = require("./app/tracks");
const users = require('./app/users');
const trackHistory = require ("./app/trackHistory");
const config = require("./config");



const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/albums', albums);
app.use('/tracks', tracks);
app.use('/artists', artists);
app.use('/users', users);
app.use('/track_history', trackHistory);




const port = 8000;


const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async (callback) => {
        await mongoose.disconnect();
        console.log('disconnect');
        callback();
    });
};

run().catch(console.error)



