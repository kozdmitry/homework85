import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";
import artistsReducer from "./reducers/artistsReducer";
import usersReducer from "./reducers/usersReducer";
import TrackHistoryReducer from "./reducers/TrackHistoryReducer";

const rootReducer = combineReducers({
    artists: artistsReducer,
    users: usersReducer,
    trackHistory: TrackHistoryReducer,
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    });
});

export default store;
