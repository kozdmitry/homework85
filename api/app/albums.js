const path = require("path");
const express = require("express");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");
const Albums = require("../models/Album");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        if (req.query.artist) {
            const albums = await Albums.find({ artist: req.query.artist })
                .populate("artist", "title")
                .sort({ year: 1 });
            return res.send(albums);
        } else {
            const albums = await Albums.find();
            return res.send(albums);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const album = await Albums.findOne({ _id: req.params.id })
            .populate("artist", "title description");
        return res.send(album);
    } catch (e) {
        res.sendStatus(500);
    }
});


router.post("/", auth, upload.single("image"), async (req, res) => {
    const albumInfo = req.body;

    try {
        if (req.file) {
            albumInfo.image = "uploads/" + req.file.filename;
        }

        const album = new Albums(albumInfo);
        await album.save();
        res.send(album);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;
