import React from 'react';
import {Container, CssBaseline} from "@material-ui/core";
import Artists from "./containers/Artists/Artists";
import {Switch, Route} from 'react-router-dom';
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";

const App = () => (
    <>
        <CssBaseline/>
        <header>
            <AppToolbar/>
        </header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={Artists} />
                    <Route path="/albums/:id" component={Albums} />
                    <Route path="/tracks/:id" component={Tracks} />
                    <Route path="/register" component={Register} />
                    <Route path="/track_history" component={TrackHistory} />
                    <Route path="/login" component={Login} />
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
