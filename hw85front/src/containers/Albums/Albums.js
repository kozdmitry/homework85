import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/artistsActions";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ArtistItem from "../Artists/ArtistItem";

const Albums = (props) => {
    const dispatch = useDispatch();
    const albums = useSelector((state) => state.artists.albums);
    console.log(albums);

    useEffect(() => {
        dispatch(fetchAlbums(props.match.params.id));
    }, [dispatch, props.match.params.id])

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography/>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {albums.map((alb) => (
                    <ArtistItem
                        key={alb._id}
                        _id={alb._id}
                        title={alb.title}
                        image={alb.image}
                        year={alb.year}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Albums;