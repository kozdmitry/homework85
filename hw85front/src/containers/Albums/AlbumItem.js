import React from 'react';
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";
import {apiURL} from "../../config";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import {CardMedia, makeStyles} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import {Link} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import Typography from "@material-ui/core/Typography";


const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const AlbumItem = ({title, image, _id, year}) => {
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + "/uploads/" + image;
    }

    return (
        <>
            <Grid item xs sm md={12} lg={3}>
                <Card className={classes.card}>
                    <CardHeader title={title}/>
                    <CardMedia
                        image={cardImage}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            Год выпуска: {year}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <IconButton component={Link} to={'/tracks' + _id}>
                            <ArrowForwardIcon />
                        </IconButton>
                    </CardActions>

                </Card>
            </Grid>
        </>


    );
};


export default AlbumItem;