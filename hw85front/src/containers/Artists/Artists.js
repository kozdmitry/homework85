import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {fetchArtists} from "../../store/actions/artistsActions";
import ArtistItem from "./ArtistItem";


const Artists = () => {

    const dispatch = useDispatch();
    const artists = useSelector((state) => state.artists.artists);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4"></Typography>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {artists.map((art) => (
                    <ArtistItem
                        key={art._id}
                        _id={art._id}
                        title={art.title}
                        image={art.image}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Artists;