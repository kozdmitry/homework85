import React from 'react';
import {Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const TracksItem = (props) => {
    return (
        <Grid contaoner direction="column" spacing={2}>
            <Paper>
                <h4>{props.title}</h4>
                <b>{props.timing}</b>
                <p>{props.number}</p>
            </Paper>
        </Grid>

    );
};

export default TracksItem;